import json
import psycopg2
from psycopg2 import Error
import flask
from flask import request, jsonify

post = flask.Flask(__name__)
post.config["DEBUG"] = True

pqslserverIP="127.0.0.1"
pqslserverport="5432"

@post.route("/examSchedule/", methods=["POST"])
def insertExamScheduleJsonObj_postman():
    ExamSchedule_data = flask.request.json
    type_of_data=type(ExamSchedule_data)
    print("Type:",type_of_data)
    print(ExamSchedule_data)
    print("Lenght of data:", len(ExamSchedule_data))
    for i in ExamSchedule_data['examschedule_details']: 
        print(i)    
    try:
        connection = psycopg2.connect(user = "postgres",
                                  password = "Arif",
                                  host = pqslserverIP,
                                  port = pqslserverport,
                                  database = "evaluate_exam")

        cursor = connection.cursor()
        for itr in ExamSchedule_data['examschedule_details']:
            create_ExamSchedule_query ="insert into ExamSchedule (examid, subjectid, examstartdatetime, examenddatetime) VALUES(%s,%s,%s,%s)"
            record_to_insert=(itr['EXAMID'],itr['SUBJECTID'],itr['EXAMSTARTDATETIME'],itr['EXAMENDDATETIME'])
            cursor.execute(create_ExamSchedule_query,record_to_insert)
            
            connection.commit()

    except (Exception, psycopg2.Error) as error :
            print ("Error while connecting to PostgreSQL", error)
    finally:
        #closing database connection.
        if(connection):
            cursor.close()
            connection.close()
            print("PostgreSQL connection is closed")
    return 0            
    
post.run(host="127.0.0.1", port=80)

#function defintion to fetch data from json object and insert into ExamSchedule                                  
def insertExamScheduleJsonObj(json_obj):
    ExamSchedule_data = json.dumps(json_obj)
    type_of_data=type(ExamSchedule_data)
    print("Type:",type_of_data)
    print(ExamSchedule_data)
    print("Lenght of data:", len(ExamSchedule_data))
    for i in ExamSchedule_data['examschedule_details']: 
        print(i)    
    try:
        connection = psycopg2.connect(user = "postgres",
                                  password = "Arif",
                                  host = pqslserverIP,
                                  port = pqslserverport,
                                  database = "evaluate_exam")

        cursor = connection.cursor()
        for itr in ExamSchedule_data['examschedule_details']:
            create_ExamSchedule_query ="insert into ExamSchedule (examid, subjectid, examstartdatetime, examenddatetime) VALUES(%s,%s,%s,%s)"
            record_to_insert=(itr['EXAMID'],itr['SUBJECTID'],itr['EXAMSTARTDATETIME'],itr['EXAMENDDATETIME'])
            cursor.execute(create_ExamSchedule_query,record_to_insert)
            
            connection.commit()

    except (Exception, psycopg2.Error) as error :
            print ("Error while connecting to PostgreSQL", error)
    finally:
        #closing database connection.
        if(connection):
            cursor.close()
            connection.close()
            print("PostgreSQL connection is closed")   

#function defintion to fetch data from json file and insert into ExamSchedule
def insertExamSchedule(json_file):
    with open(json_file, "r") as read_file:
        ExamSchedule_data = json.load(read_file)
    type_of_data=type(ExamSchedule_data)
    print("Type:",type_of_data)
    print(ExamSchedule_data)
    print("Lenght of data:", len(ExamSchedule_data))
    for i in ExamSchedule_data['examschedule_details']: 
        print(i)    
    try:
        connection = psycopg2.connect(user = "postgres",
                                  password = "Arif",
                                  host = pqslserverIP,
                                  port = pqslserverport,
                                  database = "evaluate_exam")

        cursor = connection.cursor()
        for itr in ExamSchedule_data['examschedule_details']:
            create_ExamSchedule_query ="insert into ExamSchedule (examid, subjectid, examstartdatetime, examenddatetime) VALUES(%s,%s,%s,%s)"
            record_to_insert=(itr['EXAMID'],itr['SUBJECTID'],itr['EXAMSTARTDATETIME'],itr['EXAMENDDATETIME'])
            cursor.execute(create_ExamSchedule_query,record_to_insert)
            
            connection.commit()

    except (Exception, psycopg2.Error) as error :
            print ("Error while connecting to PostgreSQL", error)
    finally:
        #closing database connection.
        if(connection):
            cursor.close()
            connection.close()
            print("PostgreSQL connection is closed")                    
