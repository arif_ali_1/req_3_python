import post
import psycopg2
from psycopg2 import Error

pqslserverIP="127.0.0.1"
pqslserverport="5432"

def PostExamSchedule(ExamSchedule_data):
    try:
        connection = psycopg2.connect(user = "postgres",
                                      password = "Arif",
                                      host = pqslserverIP,
                                      port = pqslserverport,
                                      database = "evaluate_exam")

        cursor = connection.cursor()

        print(ExamSchedule_data)
        create_ExamSchedule_query ="insert into ExamSchedule (examid, subjectid, examstartdatetime, examenddatetime) VALUES(%s,%s,%s,%s)"
        record_to_insert=(ExamSchedule_data[0],ExamSchedule_data[1],ExamSchedule_data[2],ExamSchedule_data[3]);
        cursor.execute(create_ExamSchedule_query,record_to_insert)
        connection.commit()

    except (Exception, psycopg2.Error) as error :
        print ("Error while connecting to PostgreSQL", error)
    finally:
        #closing database connection.
        if(connection):
            cursor.close()
            connection.close()
            print("PostgreSQL connection is closed")

def PutExamSchedule(new_start_time,new_end_time,exam_id):
    try:
        connection = psycopg2.connect(user = "postgres",
                                      password = "Arif",
                                      host = pqslserverIP,
                                      port = pqslserverport,
                                      database = "evaluate_exam")

        cursor = connection.cursor()

        print(newquestionid,questionid)
        create_ExamSchedule_query ="update ExamSchedule set examstartdatetime = %s, examenddatetime = %s WHERE examid = %s"
        record_to_update=(new_start_time,new_end_time,exam_id,)
        cursor.execute(create_ExamSchedule_query,record_to_update)
#        examquestionrecord = cursor.fetchall()
#        print(examquestionrecord)
        connection.commit()

    except (Exception, psycopg2.Error) as error :
        print ("Error while connecting to PostgreSQL", error)
    finally:
        #closing database connection.
        if(connection):
            cursor.close()
            connection.close()
            print("PostgreSQL connection is closed")

def GetExamSchedule(exam_id):
    try:
        connection = psycopg2.connect(user = "postgres",
                                      password = "Arif",
                                      host = pqslserverIP,
                                      port = pqslserverport,
                                      database = "evaluate_exam")

        cursor = connection.cursor()

        print(exam_id)
        fetch_ExamSchedule_query ="select * from ExamSchedule WHERE examid = %s"
        record_to_get=(exam_id,)
        cursor.execute(fetch_ExamSchedule_query,record_to_get)
        ExamSchedule_record = cursor.fetchall()
        print(ExamSchedule_record)
        connection.commit()

    except (Exception, psycopg2.Error) as error :
        print ("Error while connecting to PostgreSQL", error)
    finally:
        #closing database connection.
        if(connection):
            cursor.close()
            connection.close()
            print("PostgreSQL connection is closed")
            
def DeleteExamSchedule(exam_id):
    try:
        connection = psycopg2.connect(user = "postgres",
                                      password = "Arif",
                                      host = pqslserverIP,
                                      port = pqslserverport,
                                      database = "evaluate_exam")

        cursor = connection.cursor()

        print(exam_id)
        delete_ExamSchedule_query ="delete from ExamSchedule WHERE examid = %s"
        record_to_delete=(exam_id,)
        cursor.execute(delete_ExamSchedule_query,record_to_delete)
        delete_prepare_exam_query ="delete from examques WHERE examid = %s"
        record_to_delete=(exam_id,)
        cursor.execute(delete_prepare_exam_query,record_to_delete)
#        examquestionrecord = cursor.fetchall()
#        print(examquestionrecord)
        connection.commit()

    except (Exception, psycopg2.Error) as error :
        print ("Error while connecting to PostgreSQL", error)
    finally:
        #closing database connection.
        if(connection):
            cursor.close()
            connection.close()
            print("PostgreSQL connection is closed")
    
# Main Function         
def main(): 
    a = int(input(":: Enter you Choice ::\n"
                        " 1. POST\n 2. GET\n 3. Update \n 4. DELETE\n")) 
    if (a == 1): 
        print("POST")
        data=(203,2004,'2021-01-01 12:00:00','2021-01-01 03:00:00')
        #PostExamSchedule(data)
        #post.insertExamSchedule('post_exam_schedule.json')
        json_obj={
        "examschedule_details":[
            {
                "EXAMID":"104",
                "SUBJECTID":"1004",
                "EXAMSTARTDATETIME":"2021-01-01 12:00:00",
                "EXAMENDDATETIME":"2021-01-01 03:00:00"
            }]
            
        }
        post.insertExamScheduleJsonObj(json_obj)
    elif (a == 2):
        print("GET")
        examid=201
        GetExamSchedule(examid)
    elif (a == 3):
        print("UPDATE")
        PutExamSchedule('2021-01-01 09:00:00', '2021-01-01 12:00:00',201)
    elif (a == 4):
        print("DELETE")
        examid=201
        DeleteExamSchedule(examid)
    else: 
        raise Exception("Enter correct input") 

# Driver Code 
if __name__ == '__main__' : 
    # Calling main function 
    main() 
