import psycopg2
from psycopg2 import Error

pqslserverIP="127.0.0.1"
pqslserverport="5432"

def PostExamAns(student_exam_data):
    try:
        connection = psycopg2.connect(user = "postgres",
                                      password = "Arif",
                                      host = pqslserverIP,
                                      port = pqslserverport,
                                      database = "evaluate_exam")

        cursor = connection.cursor()

        print(student_exam_data)
        create_student_exam_ans_query ="insert into studentexamans(examid, studentid, questionid, answer) VALUES(%s,%s,%s,%s,%s)"
        record_to_insert=(student_exam_data[0],student_exam_data[1],student_exam_data[2],student_exam_data[3],)
        cursor.execute(create_student_exam_ans_query, record_to_insert)
        connection.commit()

    except (Exception, psycopg2.Error) as error :
        print ("Error while connecting to PostgreSQL", error)
    finally:
        #closing database connection.
        if(connection):
            cursor.close()
            connection.close()
            print("PostgreSQL connection is closed")

def PutExamAns(new_answer, exam_id, question_id):
    try:
        connection = psycopg2.connect(user = "postgres",
                                      password = "Arif",
                                      host = pqslserverIP,
                                      port = pqslserverport,
                                      database = "evaluate_exam")

        cursor = connection.cursor()

        print(new_answer,exam_id,question_id)
        create_student_exam_ans_query ="update studentexamans set answer= %s WHERE examid = %s and questionid=%s"
        record_to_update=(new_answer, exam_id, question_id,)
        cursor.execute(create_student_exam_ans_query, record_to_update)
#        examquestionrecord = cursor.fetchall()
#        print(examquestionrecord)
        connection.commit()

    except (Exception, psycopg2.Error) as error :
        print ("Error while connecting to PostgreSQL", error)
    finally:
        #closing database connection.
        if(connection):
            cursor.close()
            connection.close()
            print("PostgreSQL connection is closed")

def GetExamAns(exam_id):
    try:
        connection = psycopg2.connect(user = "postgres",
                                      password = "Arif",
                                      host = pqslserverIP,
                                      port = pqslserverport,
                                      database = "evaluate_exam")

        cursor = connection.cursor()

        print(exam_id)
        fetch_student_exam_ans_query ="select * from studentexamans WHERE examid = %s"
        record_to_get=(exam_id,)
        cursor.execute(fetch_student_exam_ans_query,record_to_get)
        student_exam_ans_record = cursor.fetchall()
        temp=type(student_exam_ans_record)
        print(temp)
        print(student_exam_ans_record)
        print(*student_exam_ans_record, sep = ", ")
        connection.commit()

    except (Exception, psycopg2.Error) as error :
        print ("Error while connecting to PostgreSQL", error)
    finally:
        #closing database connection.
        if(connection):
            cursor.close()
            connection.close()
            print("PostgreSQL connection is closed")
            
def DeleteExamAns(exam_id):
    try:
        connection = psycopg2.connect(user = "postgres",
                                      password = "Arif",
                                      host = pqslserverIP,
                                      port = pqslserverport,
                                      database = "evaluate_exam")

        cursor = connection.cursor()

        print(exam_id)
        delete_student_exam_ans_query ="delete from studentexamans WHERE examid = %s"
        record_to_delete=(exam_id,)
        cursor.execute(delete_student_exam_ans_query,record_to_delete)
#        examquestionrecord = cursor.fetchall()
#        print(examquestionrecord)
        connection.commit()

    except (Exception, psycopg2.Error) as error :
        print ("Error while connecting to PostgreSQL", error)
    finally:
        #closing database connection.
        if(connection):
            cursor.close()
            connection.close()
            print("PostgreSQL connection is closed")
    
# Main Function         
def main(): 
    a = int(input(":: Enter you Choice ::\n"
                        " 1. POST\n 2. GET\n 3. Update \n 4. DELETE\n")) 
    if (a == 1): 
        print("POST")
        data=[201,2001,20001,'A']
        PostExamAns(data)
    elif (a == 2):
        print("GET")
        examid=201
        GetExamAns(examid)
    elif (a == 3):
        print("UPDATE")
        PutExamAns(B,201,2001)
    elif (a == 4):
        print("DELETE")
        examid=2003
        DeleteExamAns(examid)
    else: 
        raise Exception("Enter correct input") 

# Driver Code 
if __name__ == '__main__' : 
    # Calling main function 
    main() 
