import psycopg2
from psycopg2 import Error

pqslserverIP="127.0.0.1"
pqslserverport="5432"

def PostExamQues(examquestiondata):
    try:
        connection = psycopg2.connect(user = "postgres",
                                      password = "newr00t",
                                      host = pqslserverIP,
                                      port = pqslserverport,
                                      database = "evaluate_exam")

        cursor = connection.cursor()

        print(examquestiondata)
        create_table_query ="insert into EXAMQUES (EXAMID, QUESTIONID) VALUES(%s,%s,%s)"
        record_to_insert=(examquestiondata[0],examquestiondata[1],examquestiondata[2])
        cursor.execute(create_table_query,record_to_insert)
        connection.commit()

    except (Exception, psycopg2.Error) as error :
        print ("Error while connecting to PostgreSQL", error)
    finally:
        #closing database connection.
        if(connection):
            cursor.close()
            connection.close()
            print("PostgreSQL connection is closed")

def PutExamQues(newquestionid,questionid):
    try:
        connection = psycopg2.connect(user = "postgres",
                                      password = "newr00t",
                                      host = pqslserverIP,
                                      port = pqslserverport,
                                      database = "evaluate_exam")

        cursor = connection.cursor()

        print(newquestionid,questionid)
        create_table_query ="update EXAMQUES set QUESTIONID = %s WHERE QUESTIONID = %s"
        record_to_update=(newquestionid,questionid,)
        cursor.execute(create_table_query,record_to_update)
#        examquestionrecord = cursor.fetchall()
#        print(examquestionrecord)
        connection.commit()

    except (Exception, psycopg2.Error) as error :
        print ("Error while connecting to PostgreSQL", error)
    finally:
        #closing database connection.
        if(connection):
            cursor.close()
            connection.close()
            print("PostgreSQL connection is closed")

def GetExamQues(examid):
    try:
        connection = psycopg2.connect(user = "postgres",
                                      password = "newr00t",
                                      host = pqslserverIP,
                                      port = pqslserverport,
                                      database = "evaluate_exam")

        cursor = connection.cursor()

        print(examid)
        create_table_query ="select * from EXAMQUES WHERE EXAMID = %s"
        record_to_get=(examid,)
        cursor.execute(create_table_query,record_to_get)
        examquestionrecord = cursor.fetchall()
        print(examquestionrecord)
        connection.commit()

    except (Exception, psycopg2.Error) as error :
        print ("Error while connecting to PostgreSQL", error)
    finally:
        #closing database connection.
        if(connection):
            cursor.close()
            connection.close()
            print("PostgreSQL connection is closed")
            
def DeleteExamQues(questionid):
    try:
        connection = psycopg2.connect(user = "postgres",
                                      password = "newr00t",
                                      host = pqslserverIP,
                                      port = pqslserverport,
                                      database = "evaluate_exam")

        cursor = connection.cursor()

        print(questionid)
        create_table_query ="delete from EXAMQUES WHERE QUESTIONID = %s"
        record_to_delete=(questionid,)
        cursor.execute(create_table_query,record_to_delete)
#        examquestionrecord = cursor.fetchall()
#        print(examquestionrecord)
        connection.commit()

    except (Exception, psycopg2.Error) as error :
        print ("Error while connecting to PostgreSQL", error)
    finally:
        #closing database connection.
        if(connection):
            cursor.close()
            connection.close()
            print("PostgreSQL connection is closed")
    
# Main Function         
def main(): 
    a = int(input(":: Enter you Choice ::\n"
                        " 1. POST\n 2. GET\n 3. Update \n 4. DELETE\n")) 
    if (a == 1): 
        print("POST")
        data=[3,201,2003]
        PostExamQues(data)
    elif (a == 2):
        print("GET")
        examid=201
        GetExamQues(examid)
    elif (a == 3):
        print("UPDATE")
        PutExamQues(2004,2003)
    elif (a == 4):
        print("DELETE")
        questionid=2003
        DeleteExamQues(questionid)
    else: 
        raise Exception("Enter correct input") 

# Driver Code 
if __name__ == '__main__' : 
    # Calling main function 
    main() 
